﻿<?php
// если была нажата кнопка "Отправить"
if(isset($_POST['submit']) and !empty($_POST['submit'])) {
	$name   =   substr(htmlspecialchars(trim($_POST['name'])), 0, 1000);
	$phone  =   substr(htmlspecialchars(trim($_POST['phone'])), 0, 1000000);

	$title  = "Сообщение с формы обратной связи сайта 20shoes";

	$mes = "Здравствуйте. На вашем сайте 20shoes.ru был отправлен запрос по форме обратной связи:\n";
	$mes = $mes . "Имя: " . $name . "\n";
	$mes = $mes . "Телефон: " . $phone . "\n";

	if(isset($_POST['mail']) and isset($_POST['message']) and !empty($_POST['mail']) and !empty($_POST['message']) ) {

		$email  = $_POST['mail'];
		$mess   = $_POST['message'];

		$mes = $mes . "Эл. почта: " . $email . "\n";
		$mes = $mes . "Cообщение: " . $mess . "\n";
	}

	$mes = $mes . "\n\n- -\nСообщение сгенерировано автоматически.";

	// $to - кому отправляем
	$to = 'info@20shoes.ru, mg@20shoes.ru';
	// $from - от кого
	$from='no-reply@20shoes.ru';
	
	$headers	= 'From:' . $from . "\r\n" . 'Content-type: text/plain; charset=UTF-8'  . "\r\n";
	
	// функция, которая отправляет наше письмо
	@mail($to, $title, $mes, $headers);
?>
	<div class="ajax-popup">
		<div class="thank-popup">
			<div class="thank-text">Ваша заявка принята! Я отвечу Вам в течении 30 минут. Спасибо!</div>
		</div>
	</div>
<?php
}
?>